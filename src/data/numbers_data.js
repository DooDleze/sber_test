const numbers_data = [
    {   id: 1, 
        title: 'счастливых клиентов', 
        number: 456, 
    },
    {   id: 2, 
        title: 'проекта', 
        number: 322, 
    },
    {   id: 3, 
        title: 'сайтов в топ', 
        number: 290, 
    },
    {   id: 4, 
        title: 'сайта разработано', 
        number: 132, 
    }
];

export default numbers_data;