const menu_links_data = [
    {id: 1, title: 'Главная', link: '#', active: 1},
    {id: 2, title: 'Новости', link: '#', active: 0},
    {id: 3, title: 'О компании', link: '#', active: 0},
    {id: 4, title: 'Контакты', link: '#', active: 0}
];

export default menu_links_data;