import SliderImg from '../assets/images/backgrounds/slide.png';

const slides_data = [
    {   id: 1, 
        title:"ВАШ САЙТ - ГЛАВНЫЙ БИЗНЕС-инструмент",
        text:"GO TO TOP - Ваш быстрый старт продаж", 
        linkText:"О нас", 
        link:"#", 
        img:SliderImg
    },
    {   id: 2, 
        title:"ВАШ САЙТ - ГЛАВНЫЙ БИЗНЕС-инструмент 2",
        text:"GO TO TOP - Ваш быстрый старт продаж 2", 
        linkText:"О нас", 
        link:"#", 
        img:SliderImg
    }
];

export default slides_data;