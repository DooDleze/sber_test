const news_data = [
    {   id: 1, 
        text: 'Мы начинаем этот год с наших новых разроботок в области маркетинга. Ждем Вас на наших тренингах и лекциях', 
        date: '1 января 2018', 
        link: '#', 
        img: './img/news/news1.jpg'
    },
    {   id: 2, 
        text: 'Мы работаем, вы отдыхаете! Теперь мы предоставляем полный спектр услуг по привлечению клиентов!', 
        date: '12 марта 2018', 
        link: '#', 
        img: './img/news/news2.jpg'
    },
];

export default news_data;