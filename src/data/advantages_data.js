const adv_data = [
    {   id: 1, 
        title: 'создадим продающий сайт', 
        text: 'Нету сайта? Не беда. Наши специалисты разработают оптимизированный продающий сайт для Вашей компании в минимальные сроки.', 
        img: './img/advantages/computer.png'
    },
    {   id: 2, 
        title: 'выберем аудиторию', 
        text: 'Составим аудиторию Ваших потенциальных клиентов для качественных продаж.', 
        img: './img/advantages/networking2.png'
    },
    {   id: 3, 
        title: 'настроим статистику', 
        text: 'Настроим статистику сайта, будем анализироваться действия пользователей и улучшать функционал.', 
        img: './img/advantages/settings.png'
    },
    {   id: 4, 
        title: 'разовьем соц. сети', 
        text: 'Для качественных продаж крайне необходимо вести активную деятельность в социалных сетях. Наши специалисты комплексно подойдут к вопросу привлечения клиентов через соц. сети.', 
        img: './img/advantages/networking.png'
    },
    {   id: 5, 
        title: 'минимизируем бюджет', 
        text: 'Главное не тратить деньги в пустую. Мы следим за тем, какая реклама дают максимальный результат и стремимся найти самые активные точки продаж для минимизации рекламного бюджета.', 
        img: './img/advantages/laptop.png'
    },
    {   id: 6, 
        title: 'привлечем клиентов', 
        text: 'Наша цель - максимальное количество киентов для Ваших продаж. Мы работаем по всем направления в интернет-маркетинге для того, чтобы найти именно Ваших потенциальных клиентов!', 
        img: './img/advantages/startup.png'
    },
];

export default adv_data;