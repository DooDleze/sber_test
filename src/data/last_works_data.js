const works_data = [
    {   id: 1, 
        title: 'Работа 1', 
        link: '#', 
        img: './img/works/site1.jpg'
    },
    {   id: 2, 
        title: 'Работа 2', 
        link: '#', 
        img: './img/works/site2.jpg'
    },
    {   id: 3, 
        title: 'Работа 3', 
        link: '#', 
        img: './img/works/site3.jpg'
    },
];

export default works_data;