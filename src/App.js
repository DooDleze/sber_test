import React, { Component } from 'react';
import './App.css';
import Header from './components/header/Header';
import Advantages from './components/content/advantages/Advantages';
import Promotion from './components/content/promotion/Promotion';
import Lastworks from './components/content/lastworks/Lastworks';
import Numbers from './components/content/numbers/Numbers';
import News from './components/content/news/News';
import Form from './components/content/form/Form';
import Footer from './components/footer/Footer';

// import test data
import adv_data from './data/advantages_data';
import works_data from './data/last_works_data';
import numbers_data from './data/numbers_data';
import news_data from './data/news_data';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      advantagesData: adv_data,
      promotionData: 'ХОТИТЕ НАЧАТЬ ЗАРАБАТЫВАТЬ В ИНТЕРНЕТЕ? ПРОСТО СВЯЖИТЕСЬ С НАМИ.',
      lastworksData: works_data,
      numbersData: numbers_data,
      newsData: news_data,
    };
  }

  render() {
    return (
      <div className="sber-test-app">
        <Header></Header>
        <div className="content">
          <Advantages data={this.state.advantagesData}></Advantages>
          <Promotion text={this.state.promotionData} buttonText="Связаться" buttonLink="#"></Promotion>
          <Lastworks data={this.state.lastworksData}></Lastworks>
          <Numbers data={this.state.numbersData}></Numbers>
          <News data={this.state.newsData}></News>
          <Form></Form>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
