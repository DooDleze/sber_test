import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './lastworks.css';

class Lastworks extends Component {
  render() {

    return (
      <div className="section lastworks">
        <div className="container">
            <div className="section__title">Последние работы</div>
            <div className="lastworks-items">
            {this.props.data.map((work, index) => (
                <a key={work.id} href={work.link} className="lastworks-item">
                    <img className="lastworks-item__img" src={work.img}/>
                </a>
            ))}
            </div>
        </div>
      </div>
    );
  }
}

Lastworks.propTypes = {
  data: PropTypes.array
};

export default Lastworks;
