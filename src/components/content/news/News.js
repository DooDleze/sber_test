import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './news.css';
import NewsItem from './NewsItem';

class News extends Component {
  render() {

    return (
      <div className="section news">
        <div className="container">
            <div className="section__title">Новости</div>
            <div className="news-items">
            {this.props.data.map((item, index) => (
                <NewsItem key={item.id} data={item}></NewsItem>
            ))}
            </div>
        </div>
      </div>
    );
  }
}

News.propTypes = {
    data: PropTypes.array
};

export default News;
