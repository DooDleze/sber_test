import React, { Component } from 'react';
import PropTypes from 'prop-types';

const NewsItem = function NewsItem(props) {
    return (
        <div className="news-item">
            <img className="news-item__img" src={props.data.img}/>
            <div className="news-item__group_content">
                <div className="news-item__date">{props.data.date}</div>
                <div className="news-item__text">{props.data.text}</div>
                <a href={props.data.link} className="news-item__link">Подробнее</a>
            </div>
        </div>
    );
}

NewsItem.propTypes = {
    data: PropTypes.shape({
        img: PropTypes.string,
        date: PropTypes.string,
        text: PropTypes.string,
        link: PropTypes.string,
    })
};


export default NewsItem;