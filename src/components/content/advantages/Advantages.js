import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './advantages.css';
import AdvantageItem from './AdvantageItem';

class Advantages extends Component {
  render() {
    return (
      <div className="section advantages">
        <div className="container">
            <div className="advantages-items">
            {this.props.data.map((advantage, index) => (
                <AdvantageItem key={advantage.id} content={advantage}></AdvantageItem>
            ))}
            </div>
        </div>
      </div>
    );
  }
}

Advantages.propTypes = {
  data: PropTypes.array
};

export default Advantages;
