import React, { Component } from 'react';
import PropTypes from 'prop-types';

const AdvantageItem = function AdvantageItem(props) {
    return (
        <div className="advantages-item">
            <img className="advantages-item__img" src={props.content.img}/>
            <div className="advantages-item__title">{props.content.title}</div>
            <div className="advantages-item__text">{props.content.text}</div>
        </div>
    );
}

AdvantageItem.propTypes = {
    content: PropTypes.shape({
        img: PropTypes.string,
        title: PropTypes.string,
        text: PropTypes.string,
    })
};

export default AdvantageItem;