import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './numbers.css';

class Numbers extends Component {
  render() {
    return (
      <div className="section blue numbers">
        <div className="container">
            <div className="numbers-items">
              {this.props.data.map((number, index) => (
                  <div key={number.id} className="numbers-item">
                    <div className="numbers-item__number">{number.number}</div>
                    <div className="numbers-item__title">{number.title}</div>
                  </div>
              ))}
            </div>
        </div>
      </div>
    );
  }
}

Numbers.propTypes = {
  data: PropTypes.array
};

export default Numbers;
