import React, { Component } from 'react';
import './form.css';

class Form extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        fio: '',
        email: '',
        phone: '',
        text: ''
      };
  
      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleInputChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;
  
      this.setState({
        [name]: value
      });
    }
  
    handleSubmit(event) {
        event.preventDefault();
        alert('Форма отправлена!: ' + this.state.fio + ', '+ this.state.email + ', '+ this.state.phone);
    }
    

    render() {
      return (
        <div className="section gray contact-form">
            <div className="container">
                <div className="section__title">Напишите нам</div>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-12 col-md-6 mb-2">
                            <div className="contact-form__input-wrapper">
                                <input
                                  required
                                  placeholder="ФИО"
                                  name="fio"
                                  type="text"
                                  value={this.state.fio}
                                  onChange={this.handleInputChange} />
                            </div>
                            <div className="contact-form__input-wrapper">
                                <input
                                  required
                                  placeholder="Email"
                                  name="email"
                                  type="text"
                                  value={this.state.email}
                                  onChange={this.handleInputChange} />
                            </div>
                            <div className="contact-form__input-wrapper">
                                <input
                                  required
                                  placeholder="Телефон"
                                  name="phone"
                                  type="text"
                                  value={this.state.phone}
                                  onChange={this.handleInputChange} />
                            </div>
                        </div>
                        <div className="col-12 col-md-6 mb-2">
                            <textarea
                              required
                              placeholder="Ваше сообщение"
                              name="text"
                              value={this.state.text}
                              onChange={this.handleInputChange} />
                        </div>
                        <div className="col-12 col-md-6 mb-2">
                            <button className="contact-form__btn" type="submit">Отправить вопрос</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      );
    }
  }

export default Form;
