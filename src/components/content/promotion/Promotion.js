import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './promotion.css';
import Button from '../../ui/Button';

class Promotion extends Component {
  
  render() {
    return (
        <div className="section blue promotion">
            <div className="container">
                <div className="promotion__text">{this.props.text}</div>
                <Button link={this.props.buttonLink} text={this.props.buttonText}></Button>
            </div>
        </div>
    );
  }
}


Promotion.propTypes = {
  text: PropTypes.string,
  buttonLink: PropTypes.string,
  buttonText: PropTypes.string,
};

export default Promotion;
