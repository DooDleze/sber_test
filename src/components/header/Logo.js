import React, { Component } from 'react';
import logoImage from '../../assets/images/icons/web.png';
import './logo.css';

const Logo = function Logo() {
    return (
        <div className="logo">
            <img src={logoImage} alt="logo" title="logo" />
            <div className="logo__text">GO TO <span>TOP</span></div>
        </div>
    );
}

export default Logo;