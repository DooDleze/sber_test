import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './TopMenu.css';


class TopMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cssclass: '',
    };

    this.activateMenu = this.activateMenu.bind(this);
  }

  activateMenu(e) {
      e.preventDefault();
      if(this.state.cssclass === ''){
        this.setState({
          cssclass: 'active'
        })
      }else{
        this.setState({
          cssclass: ''
        })
      }
  }

  render() {

    const content = this.props.data.map((link) =>
      <li key={link.id}>
        <a className={link.active ? 'active' : ''} href={link.link} >
          {link.title}
        </a>
      </li>
    );

    const cssClassWrapper = 'menu-wrapper '+this.state.cssclass;

    return (
        <div className={cssClassWrapper}>
          <div className="gamburger" onClick={this.activateMenu}>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <ul className="top-menu">
              {content}
          </ul>
        </div>
    );
  }
}

TopMenu.propTypes = {
  data: PropTypes.array
};

export default TopMenu;
