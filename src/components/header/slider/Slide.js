import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../ui/Button';

const Slide = function Slide(props) {
    return (
        <div className="main-slider-item" style={{ backgroundImage: `url(${props.slide.img})`}}>
            <div className="main-slider-item__content">
                <div className="main-slider-item__title">{props.slide.title}</div>
                <div className="main-slider-item__text">{props.slide.text}</div>
                <Button link={props.slide.link} text={props.slide.linkText}></Button>
            </div>
        </div>
    );
}

Slide.propTypes = {
    slide: PropTypes.shape({
        id: PropTypes.number,
        img: PropTypes.string,
        title: PropTypes.string,
        text: PropTypes.string,
        link: PropTypes.string,
        linkText: PropTypes.string
    })
};

export default Slide;