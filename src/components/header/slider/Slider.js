import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Slider.css';
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css'
import Slide from './Slide';

class Slider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slides: props.data,
            virtualData: {
                slides: [],
            },
        }
    }
    componentDidMount() {
        const self = this;
        const swiper = new Swiper('.swiper-container', {
            // autoplay: {
            //     delay: 3000,
            //     disableOnInteraction: false,
            // },
            loop: true,
            navigation: {
                nextEl: '.main-slider-next',
                prevEl: '.main-slider-prev',
            },
            virtual: {
                slides: self.state.slides,
                renderExternal(data) {
                    // assign virtual slides data
                    self.setState({
                        virtualData: data,
                    });
                }
            },
        });
    }
    render() {
        return (
        <div className="main-slider">
            <div className="swiper-container">
                <div className="swiper-wrapper">
                    {this.state.virtualData.slides.map((slide, index) => (
                        <div className="swiper-slide" key={index}>
                            <Slide key={slide.id} slide={slide}></Slide>
                        </div>
                    ))}
                </div>
                <div className="main-slider-next"></div>
                <div className="main-slider-prev"></div>
            </div>
        </div>
        )
    }
}

Slider.propTypes = {
    data: PropTypes.array
};
  
export default Slider;