import React, { Component } from 'react';
import './header.css';
import TopMenu from './TopMenu';
import Slider from './slider/Slider';
import Logo from './Logo';

import menu_links_data from '../../data/menu_data';
import slides_data from '../../data/slides_data';

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cssclass: '',
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    let scrollTop = window.scrollY;
    let tempclass = '';
    if(scrollTop > 200){
      tempclass = 'min'
    }
    if(this.state.cssclass != tempclass){
      this.setState({
        cssclass: tempclass
      });
    }
   
  }

  render() {
    let headerClass = 'header ' + this.state.cssclass;
    return (
      <div className="top-content">
        <div className={headerClass}>
            <div className="container">
                <Logo></Logo>
                <TopMenu data={menu_links_data}></TopMenu>
            </div>
        </div>
        <Slider data={slides_data}></Slider>
      </div>
    );
  }
}

export default Header;
