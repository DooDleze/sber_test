import React, { Component } from 'react';
import './footer.css';
import Texticon from '../ui/Texticon';

import mapIcon from '../../assets/images/icons/placeholder.png';
import phoneIcon from '../../assets/images/icons/telephone.png';
import mailIcon from '../../assets/images/icons/mail.png';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-4 col-lg-3">
                    <div className="footer__title">О нас</div>
                    <div className="footer__about-text">
                        Мы оказываем всестороннюю помощь компаниям и физическим лицам — собственникам веб-ресурсов, которые готовы использовать сайт, как эффективный рекламный инструмент, позволяющий развивать бизнес.
                    </div>
                </div>
                <div className="col-12 col-md-4 col-lg-3 offset-lg-2">
                    <div className="footer__title">Читайте в новостях</div>
                    <a href="#">Новые разработки</a>
                    <a href="#">Мы работаем, вы отдыхаете</a>
                </div>
                <div className="col-12 col-md-4 col-lg-3 offset-lg-1">
                    <div className="footer__title">Контакты</div>
                    <Texticon img={mapIcon} text={"Москва, Большая Спасская 12"}></Texticon>
                    <Texticon img={phoneIcon} text={"8 (495) 626-46-00"}></Texticon>
                    <Texticon img={mailIcon} text={"info@gototop.com"}></Texticon>
                </div>
            </div>
        </div>
        <div className="copyright">2018 Все права защищены</div>
      </footer>
    );
  }
}

export default Footer;
