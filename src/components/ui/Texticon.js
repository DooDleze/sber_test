import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './texticon.css';

class Texticon extends Component {
    render() {
        return (
            <div className="text-icon">
                <img src={this.props.img} className="text-icon__img" />
                <span>{this.props.text}</span>
            </div>
        );
    }
}

Texticon.propTypes = {
    img: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
};

export default Texticon;
