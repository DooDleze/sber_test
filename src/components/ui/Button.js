import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './button.css';

class Button extends Component {
    render() {
        return (
            <a className="btn" href={this.props.link}>
                {this.props.text}
            </a>
        );
    }
}

Button.propTypes = {
    link: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
};

export default Button;
